//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.enigma;

import java.io.*;


public class Enigma {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        boolean working = true;

        while (working) {
            String line = null;
            System.out.println("type command: ");
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String[] splitLine = line.split(" ");
            Action action = Action.getAction(commandCheck(splitLine));
            String dirPath = "c:/enigma/";
            String inputPath = "";
            String outputPath = "";

            if(splitLine.length>=3) {
                inputPath = dirPath + (splitLine[2]) + ".txt";
                outputPath = dirPath + (splitLine[2]) + ".src";
            }

            switch (action){

                case ENCRYPTION:
                    Cryption encryptionAction = Cryption.getCryptionMethod(splitLine[1]);

                    switch (encryptionAction){

                        case CAESAR:
                            writeFileContent(outputPath, Cryption.CAESAR.caesarCryption(getFileContent(inputPath),3));
                            progressBar();
                            break;
                        case XOR:
                            System.out.println("encryption by xor");
                            break;
                        case ENIGMA:
                            System.out.println("encryption by  enigma");
                            break;
                    }
                    break;
                case DECRYPTION:
                    Cryption decryptionAction = Cryption.getCryptionMethod(splitLine[1]);

                    switch (decryptionAction){

                        case CAESAR:
                            writeFileContent(inputPath, Cryption.CAESAR.caesarCryption(getFileContent(outputPath),-3));
                            progressBar();
                            break;
                        case XOR:
                            System.out.println("decryption by xor");
                            break;
                        case ENIGMA:
                            System.out.println("decryption by  enigma");
                            break;
                    }
                    break;
                case HELP:
                    instruction();
                    break;
                case INCORRECT:
                    System.out.println("incorrect command, for HELP type \"help\".");
                    break;
                case QUIT:
                    working = false;
                    break;
            }
        }
    }

    private static String commandCheck(String[] tab) {
        String command = null;
        try {
            if (tab[0].equalsIgnoreCase("quit") || tab[0].equalsIgnoreCase("q")) {
                return command = "q";
            } else if (tab[0].equalsIgnoreCase("ec") && tab.length == 3) {
                for (int i = 0; i < Cryption.values().length; i++) {
                    if(tab[1].equalsIgnoreCase(Cryption.values()[i].name())){
                        return command = "ec";
                    }
                }
                return command = "ic";
            } else if (tab[0].equalsIgnoreCase("dc") && tab.length == 3) {
                for (int i = 0; i < Cryption.values().length; i++) {
                    if(tab[1].equalsIgnoreCase(Cryption.values()[i].name())){
                        return command = "dc";
                    }
                }
                return command = "ic";
            } else if (tab[0].equalsIgnoreCase("help")) {
                return command = "help";
            } else {
                return command = "ic";
            }
        }catch (ArrayIndexOutOfBoundsException e){
            return command = "ic";
        }
    }

    private static String getFileContent(String path) {
        StringBuilder content = new StringBuilder();
        File file = new File(path);
        if (file.exists() && file.isFile() && file.canRead()) {
            try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))){
                while (reader.ready()){
                    content.append(reader.readLine());
                    content.append("\n");
                }
            } catch (IOException e){
                e.printStackTrace();
            }
        }

        return content.toString();
    }

    private static void writeFileContent(String path, String content){
        File file = new File(path);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))){
                writer.write(content);
                writer.flush();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private static void instruction() {
        System.out.println("type command :\n" +
                "ec [caesar] | [xor] | [enigma] \"file name\" - for encryption file\n" +
                "dc [caesar] | [xor] | [enigma] \"file name\" - for decryption file\n" +
                "help - show this menu\n" +
                "q || quit - exit program.");
    }

    private static void progressBar(){
        System.out.print("in progress: ");
        for(int i =0; i<10; i++){
            System.out.print((char)187);
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.print("  DONE!\n");
    }

}
