//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.enigma;

public interface CryptionMethods {

    public String caesarCryption(String toCryption, int enOrDe);

    public String xorCryption(String toCryption, int enOrDe);

    public String enigmaCryption(String toCryption, int enOrDe);
}
