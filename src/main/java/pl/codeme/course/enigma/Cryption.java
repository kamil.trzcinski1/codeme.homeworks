//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.enigma;

public enum Cryption implements CryptionMethods {
    CAESAR("caesar") {

    },
    XOR("xor") {

    },
    ENIGMA("enigma") {

    };

    String cryptionMethod;

    Cryption(String cryptionMethod) {
        this.cryptionMethod = cryptionMethod;
    }

    public static Cryption getCryptionMethod(String text) {

        for (Cryption item : Cryption.values()) {
            if (item.cryptionMethod.equalsIgnoreCase(text)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public String caesarCryption(String toCryption, int enOrDe) {
        char[] temp = toCryption.toCharArray();
        String crypted = "";
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] == '\n' || temp[i] == '.' || temp[i] == ',' || temp[i] == '?'){
                crypted += temp[i];
            }else {
                crypted += (char) (temp[i] + enOrDe);
            }
        }
        return crypted;
    }

    @Override
    public String xorCryption(String toCryption, int enOrDe) {
        return null;
    }

    @Override
    public String enigmaCryption(String toCryption, int enOrDe) {
        return null;
    }
}
