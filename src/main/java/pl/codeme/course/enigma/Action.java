//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.enigma;

public enum Action {
    ENCRYPTION("ec"),
    DECRYPTION("dc"),
    HELP("help"),
    INCORRECT("ic"),
    QUIT("q")
    ;

    String action;

    Action(String action) {
        this.action = action;
    }

    public static Action getAction(String text) {

        for (Action item : Action.values()) {
            if (item.action.equalsIgnoreCase(text)) {
                return item;
            }
        }
        return null;
    }
}
