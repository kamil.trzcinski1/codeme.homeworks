//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.calculator;

public enum Action {
    ADD("+") {
        @Override
        double operation(double a, double b) {
            return a + b;
        }
    },
    SUBTRACT("-") {
        @Override
        double operation(double a, double b) {
            return a - b;
        }
    },
    MULTIPLY("*") {
        @Override
        double operation(double a, double b) {
            return a * b;
        }
    },
    DIVIDE("/") {
        @Override
        double operation(double a, double b) {
            return a / b;
        }
    };

    String action;

    Action(String action) {
        this.action = action;
    }

    public static Action getAction(String text) {

        for (Action item : Action.values()) {
            if (item.action.equalsIgnoreCase(text)) {
                return item;
            }
        }
        return null;
    }

    double operation(double a, double b) {
        return 0;
    }
}
