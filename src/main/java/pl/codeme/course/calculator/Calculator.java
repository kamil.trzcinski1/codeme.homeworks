//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.calculator;

import pl.codeme.course.calculator.action.lambda.ActionLambda;
import pl.codeme.course.calculator.action.lambda.InterfaceCalculation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calculator {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {

            System.out.println("enter action, for exit type \"q\"");
            String line = null;
            String[] splitLine = null;
            double a = 0;
            double b = 0;
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(line.equalsIgnoreCase("q")){
                break;
            }
            splitLine = spliter(line);

            if (splitLine.length > 1) {
                a = Double.parseDouble(splitLine[0]);
                b = Double.parseDouble(splitLine[1]);
            }

//            using Enum Action
//            Action action = Action.getAction(checkChar(line));
//            switch (action){
//
//                case ADD:
//                    System.out.println(action.operation(a,b));
//                    break;
//                case SUBTRACT:
//                    System.out.println(action.operation(a,b));
//                    break;
//                case MULTIPLY:
//                    System.out.println(action.operation(a,b));
//                    break;
//                case DIVIDE:
//                    System.out.println(action.operation(a,b));
//                    break;
//            }

//            using Enum ActionLambda
            InterfaceCalculation operation = ActionLambda.getActionLambdaByText(checkChar(line));

            System.out.println(operation.calculation(a,b));
        }
    }

    static String[] spliter(String text) {
        String[] tmp = null;

        if (text.contains("+")) {
            tmp = text.split("[+]");
        } else if (text.contains("*")) {
            tmp = text.split("[*]");
        } else if (text.contains("/")) {
            tmp = text.split("[/]");
        } else if (text.contains("-")) {
            if (text.startsWith("-")) {
                text = text.substring(1, text.length());
                tmp = text.split("[-]");
                tmp[0] = "-" + tmp[0];
            } else {
                tmp = text.split("[-]");
            }
            if(tmp[1].equals("")){
                tmp[1]="-"+tmp[2];
            }
        } else {
            tmp = text.split("");
        }

        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = tmp[i].replace(",", ".");
        }

        return tmp;
    }

    static String checkChar(String text) {
        String c = null;
        if (text.contains("+")) {
            c = "+";
        } else if (text.contains("*")) {
            c = "*";
        } else if (text.contains("/")) {
            c = "/";
        } else if (text.contains("-")) {
            c = "-";
        } else if (text.contains("q")) {
            c = "q";
        }

        return c;
    }
}
