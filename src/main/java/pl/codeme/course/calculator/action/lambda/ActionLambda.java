package pl.codeme.course.calculator.action.lambda;

public enum ActionLambda {
    ADD("+"),
    SUBTRACT("-"),
    MULTIPLY("*"),
    DIVIDE("/");

    String action;

    ActionLambda(String action) {
        this.action = action;
    }

    public static InterfaceCalculation getActionLambdaByText(String text) {

        for (ActionLambda item : ActionLambda.values()) {
            if (item.action.equalsIgnoreCase(text)) {
                switch (item) {

                    case ADD:
                        return ((v1, v2) -> v1+v2);
                    case SUBTRACT:
                        return ((v1, v2) -> v1-v2);
                    case MULTIPLY:
                        return ((v1, v2) -> v1*v2);
                    case DIVIDE:
                        return ((v1, v2) -> v1/v2);
                }
            }
        }
        return null;
    }
}
