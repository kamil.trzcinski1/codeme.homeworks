package pl.codeme.course.calculator.action.lambda;

@FunctionalInterface
public interface InterfaceCalculation {

    double calculation(double v1, double v2);

}