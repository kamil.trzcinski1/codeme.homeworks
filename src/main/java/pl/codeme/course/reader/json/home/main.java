//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.reader.json.home;

import javax.json.*;
import java.io.*;
import java.util.ArrayList;


public class main {
    public static void main(String[] args) throws Exception{

        JsonReader reader = Json.createReader( new FileInputStream("c:/temp/users.json"));
        JsonObject userObject = reader.readObject();
        reader.close();

        JsonArray usersArray = userObject.getJsonArray("users");

        ArrayList<User> users = new ArrayList<>();
        for (JsonValue jsonValue : usersArray) {
            User user = new User((jsonValue.asJsonObject().getString("name")), jsonValue.asJsonObject().getString("email"));
            users.add(user);
        }

        for (User user : users) {
            System.out.println(user.toString());
        }
    }
}
