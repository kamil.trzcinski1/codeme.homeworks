//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.reader.json.home;

public class User {

    private String name;
    private String email;

    public User(String name, String email){
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String toString(){
        return ("user name:  " + name +" || " + "user email: " + email);
    }
}
