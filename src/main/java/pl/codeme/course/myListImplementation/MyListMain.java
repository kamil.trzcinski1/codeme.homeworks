//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.myListImplementation;

import java.util.Random;

public class MyListMain {
    public static void main(String[] args) {
        MyList myList = new MyList();
        Random random = new Random();

        for (int i = 0; i<5; i++){
            myList.append(random.nextInt(3));
        }

        myList.append("ala").append(new Object[4]).append(323.22).append('%');
        myList.print();
        System.out.println();
        myList.print(6);
    }
}
