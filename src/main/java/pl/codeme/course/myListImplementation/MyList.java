//  @author kamil.trzcinski#gmail.com

package pl.codeme.course.myListImplementation;

//using the interface as part of the exercise
public class MyList implements Appendable {

    private Object[] myArray = new Object[1];
    private int index = 0;

    private int getIndex() {
        return index;
    }

    public Object[] getMyArray() {
        return myArray;
    }

    public Object getMyArray(int index) {
        return myArray[index];
    }

    private void setMyArray(Object[] myArray) {
        this.myArray = myArray;
    }

    @Override
    public Appendable append(Object item) {
        try {
            myArray[getIndex()] = item;
        } catch (ArrayIndexOutOfBoundsException e) {
            Object[] myNewArray = new Object[myArray.length + 1];
            if (myArray.length >= 0) System.arraycopy(myArray, 0, myNewArray, 0, myArray.length);
            setMyArray(myNewArray);
            myArray[getIndex()] = item;
        }
        this.index++;
        return this;
    }

    @Override
    public Appendable append(Object item, int index) {
        myArray[index] = item;
        return this;
    }

    @Override
    public Appendable remove() {
        Object[] myNewArray = new Object[myArray.length - 1];
        System.arraycopy(myArray, 0, myNewArray, 0, myNewArray.length);
        setMyArray(myNewArray);

        this.index--;
        return this;
    }

    @Override
    public Appendable remove(int idx) {
        Object[] myNewArray = new Object[myArray.length - 1];
        int j = myNewArray.length;
        for (int i = 0; i < myArray.length; i++) {
            if (i == idx) {
                continue;
            } else {
                myNewArray[j] = myArray[i];
                j++;
            }
            setMyArray(myNewArray);
        }
        this.index--;
        return this;
    }

    public void print() {
        for (int i = 0; i < getIndex(); i++) {
            System.out.println("index nr: " + i +" = " + myArray[i]);
        }
    }

    public void print(int idx) {
        System.out.println("index nr: " + idx +" = " + myArray[idx]);
    }
}
